# README #

Telstra4GXMonitor - monitor your Telstra branded Netgear hotspot and 4GX routers from your Windows taskbar

### What is this for? ###

* Telstra4GXMonitor is an app I hacked out in an afternoon or two because I didn't always want to use the mobile app or login the web interface.
* The UI is blatantly copied from that brilliant app Internode MUM
* Version 1.0 and likely to stay that way

### How do I get set up? ###

* Built in Visual Studio 2017 with .NET 4.7
* There shouldn't be anything too 4.7 specific, though there's some C#7-isms, IIRC
* I was lazy and hard-coded the URL into UsageData.cs. Yes, that should be through a config tab/menu/option. Volunteers?
* Compile then run the .EXE. Put into your Startup if you want to have it come up automatically with Windows.

### TODO ###

* Configuration/options/preferences
* Smarter JSON verification in case Netgear models vary
* Unit tests - would be nice. 

### Author ###

* Michael Hoffmann (michaelh@centaur.id.au)

### Stealing It ###

If, as now increasingly happens, you feel like taking my code and pretending you wrote it, be my guest. Says more about you, that you have to steal a quick&dirty hack.

However, if I ever see this branded by Telstra or Netgear, I might consider asking a lawyer about it. OTOH, if Telstra or Netgear feel like suing me for sneaking 
about in their API, they their lawyers may ask me for every cent I made on this program as compensation. Don't spend it all at once, guys!

### Credit ###

The UI is a vastly inferior version of Internode MUM by Angus Johnson. A brilliant and useful tool, a must for any Internode customer! I wish I had the time and drive
to get more of his long-term monitoring and averaging in.

### License ###

I'll get around to deciding. Whichever one lets you take it and modify as long as you give credit and pull request back your changes.