using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telstra4GXMonitor.Properties;

namespace Telstra4GXMonitor
{
    public class TelstraApplicationContext : ApplicationContext
    {
        // Should be half an hour in milliseconds
        private const int LoopDelay = 1800000;

        private readonly UsageForm _myUsageForm = new UsageForm();
        private readonly NotifyIcon _trayIcon;
        private UsageData _usageData;
        private UsageData.UsageStatus _usageStatus;
        // private int _usageStatus;

        public TelstraApplicationContext()
        {
            // Initialize Tray Icon
            _trayIcon = new NotifyIcon()
            {
                Icon = Resources.MyIcon,
                ContextMenu = new ContextMenu(
                    new MenuItem[]
                    {
                        new MenuItem("Exit", Exit)
                    }),
                Visible = true

            };
            _trayIcon.DoubleClick += ShowGraphs;
            DoWorkAsyncInfiniteLoop();
        }

        private async Task DoWorkAsyncInfiniteLoop()
        {
            _usageData = new UsageData();

            while (true)
            {
                _usageStatus = _usageData.UpdateValues();

                // update the tray icon mouse-over text
                SetNotifyIconText(_trayIcon, 
                    $@"Amount Left: {_usageData.DataRemainingMb:N1} MB ({_usageData.PercentageRemaining:D}%)
Days left: {_usageData.TimeRemaining:%d} ({_usageData.TimePercentageRemaining:D}%)
Updated: {_usageData.LastUpdated:t} Quality: {_usageData.SignalQuality}");
                // update the icon
                // Brush brushColour = ColourGradients.BrushColour(_usageStatus);
                Brush brushColour;
                switch (_usageStatus)
                {
                    case UsageData.UsageStatus.Normal:
                        brushColour = Brushes.LimeGreen;
                        break;
                    case UsageData.UsageStatus.Minimal:
                        brushColour = Brushes.GreenYellow;
                        break;
                    case UsageData.UsageStatus.Minor:
                        brushColour = Brushes.Yellow;
                        break;
                    case UsageData.UsageStatus.Notable:
                        brushColour = Brushes.Orange;
                        break;
                    case UsageData.UsageStatus.Major:
                        brushColour = Brushes.DarkOrange;
                        break;
                    case UsageData.UsageStatus.Critical:
                        brushColour = Brushes.Red;
                        break;
                    case UsageData.UsageStatus.Yikes:
                        brushColour = Brushes.DarkRed;
                        break;
                    default:
                        brushColour = Brushes.LimeGreen;
                        break;
                }
                UpdateIcon(_usageData.PercentageRemaining, _usageData.SignalBars, brushColour);

                // don't run again for at least half an hour
                await Task.Delay(LoopDelay);
            }
        }

        private static void SetNotifyIconText(NotifyIcon ni, string text)
        {
            if (text.Length >= 128)
                throw new ArgumentOutOfRangeException("Text limited to 127 characters");

            var t = typeof(NotifyIcon);
            const BindingFlags hidden = BindingFlags.NonPublic | BindingFlags.Instance;
            var memberInfo = t.GetField("text", hidden);

            if (memberInfo != null)
                memberInfo.SetValue(ni, text);

            var fieldInfo = t.GetField("added", hidden);
            if (fieldInfo != null && (bool)fieldInfo.GetValue(ni))
                t.GetMethod("UpdateIcon", hidden).Invoke(ni, new object[] { true });
        }

        private void Exit(object sender, EventArgs e)
        {
            // Hide tray icon, otherwise it will remain shown until user mouses over it
            _trayIcon.Visible = false;

            Application.Exit();
        }

        private void ShowGraphs(object sender, EventArgs e)
        {
            _myUsageForm.FillUsageFormData(_usageData);

            // If we are already showing the window, merely focus it.
            if (_myUsageForm.Visible)
            {
                _myUsageForm.Activate();
            }
            else
            {
                _myUsageForm.ShowDialog();
            }
        }

        // Needed to fend off a possible memory  leak and get rid of icon handle
        [System.Runtime.InteropServices.DllImport("user32.dll", CharSet = CharSet.Auto)]
        static extern bool DestroyIcon(IntPtr handle);

        private void UpdateIcon(int usageRemaining, int signal, Brush brushColour = null)
        {
            brushColour = brushColour ?? Brushes.LimeGreen;
            // Make font slightly smaller to fit 3 digits for 100% in
            var fontSize = (usageRemaining == 100) ? 10 : 12;

            using (var bmp = new Bitmap(16, 16))
            {
                using (var g = Graphics.FromImage(bmp))
                {
                    g.SmoothingMode = SmoothingMode.AntiAlias;
                    g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    g.PixelOffsetMode = PixelOffsetMode.HighQuality;
                    g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SingleBitPerPixelGridFit;
                    DrawSignalBars(g, brushColour, bmp.Width, signal);

                    // Draw the text onto the image
                    var usageRemainingAsString = $@"{usageRemaining:D2}";
                    var fontToUse = new Font("Tahoma", fontSize, FontStyle.Regular, GraphicsUnit.Pixel);
                    g.DrawString(usageRemainingAsString, fontToUse, Brushes.White, 0, 4);

                    g.Flush();
                }
                var hicon = bmp.GetHicon();
                var myIcon = Icon.FromHandle(hicon);

                _trayIcon.Icon = myIcon;
                DestroyIcon(myIcon.Handle);
            }
        }

        private static void DrawSignalBars(Graphics g, Brush brushColour, int width, int signal)
        {
            var blipWidth = width / 5.0f;
            var barWidth = blipWidth * 0.9f;
            for (var i = 0; i < signal; i++)
            {
                var rectf = new RectangleF(0+i*blipWidth, 0, barWidth, 5);
                g.FillRectangle(brushColour, rectf);
            }
        }
    }
}