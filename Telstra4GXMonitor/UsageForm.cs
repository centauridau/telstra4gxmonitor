﻿using System;
using System.Drawing;
using System.Windows.Forms;
// ReSharper disable PossibleLossOfFraction

namespace Telstra4GXMonitor
{
    public partial class UsageForm : Form
    {
        private UsageData.UsageStatus _usagestatus;
        // private int _usagestatus;
        private int _days = 100;
        private int _usage = 100;
        private readonly Font _barFont = new Font("Arial", 8, FontStyle.Bold);
        private UsageData _usageData;

        public UsageForm()
        {
            InitializeComponent();
        }

        private void UsageForm_Load(object sender, EventArgs e)
        {
            var x = Screen.PrimaryScreen.WorkingArea.Right - Width;
            var y = Screen.PrimaryScreen.WorkingArea.Bottom - Height;
            DesktopLocation = new Point(x, y);
        }

        public void FillUsageFormData(UsageData usageData)
        {
            _usageData = usageData;
            dataTransferred.Text = $@"{_usageData.DataUsedMb:N1} MB";
            lastUpdated.Text = $@"{_usageData.LastUpdated:g}";
            monthlyQuota.Text = $@"{_usageData.MonthlyQuotaMb:N1} MB";
            nextBillingDate.Text = $@"{_usageData.NextBillingDate:M}";
            startOfMonth.Text = $@"{_usageData.DailyUsageRecommendedMb:N1} MB";
            soFarMonth.Text = $@"{_usageData.DataUsedSoFarPerDayMb:N1} MB";
            remainingDailyUsage.Text = $@"{_usageData.DailyUsageRemainingMb:N1} MB";
            signalBand.Text = $@"{_usageData.SignalBand}";
            _usage = _usageData.PercentageRemaining;
            _days = _usageData.TimePercentageRemaining;
            _usagestatus = _usageData.CurrentUsageStatus;
        }

        private void OkayButton_Click(object sender, EventArgs e)
        {
            // tight coupling?
            _usagestatus = _usageData.UpdateValues();
            Close();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);

            PrintPercentInBar(daysBar, _days);
            PrintPercentInBar(usageBar, _usage);
        }

        private void PrintPercentInBar(ProgressBar progressBar, int percent)
        {
            progressBar.ForeColor = BarColour() ;
            progressBar.Value = percent;
            var percentString = $"{percent}%";
            progressBar.Refresh();
            using (var gr = progressBar.CreateGraphics())
            {
                gr.DrawString( percentString, _barFont, Brushes.Black,
                    new PointF(progressBar.Width / 2 - (gr.MeasureString(percentString, _barFont).Width / 2.0F), 
                                progressBar.Height / 2 - (gr.MeasureString(percentString, _barFont).Height / 2.0F)));
            }
        }

        private Color BarColour()
        {
            Color barColour;
            switch (_usagestatus)
            {
                case UsageData.UsageStatus.Normal:
                    barColour = Color.LimeGreen;
                    break;
                case UsageData.UsageStatus.Minimal:
                    barColour = Color.GreenYellow;
                    break;
                case UsageData.UsageStatus.Minor:
                    barColour = Color.Yellow;
                    break;
                case UsageData.UsageStatus.Notable:
                    barColour = Color.Orange;
                    break;
                case UsageData.UsageStatus.Major:
                    barColour = Color.DarkOrange;
                    break;
                case UsageData.UsageStatus.Critical:
                    barColour = Color.Red;
                    break;
                case UsageData.UsageStatus.Yikes:
                    barColour = Color.DarkRed;
                    break;
                default:
                    barColour = Color.LimeGreen;
                    break;
            }
            return barColour;
        }
    }
}
