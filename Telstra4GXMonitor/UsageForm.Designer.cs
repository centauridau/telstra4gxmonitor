﻿namespace Telstra4GXMonitor
{
    partial class UsageForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.meteredDailyAveragesBox = new System.Windows.Forms.GroupBox();
            this.soFarMonth = new System.Windows.Forms.Label();
            this.startOfMonth = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.remainingDailyUsage = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.meteredUsageBox = new System.Windows.Forms.GroupBox();
            this.dataTransferred = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.planBox = new System.Windows.Forms.GroupBox();
            this.nextBillingDate = new System.Windows.Forms.Label();
            this.monthlyQuota = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.lastUpdatedStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.lastUpdated = new System.Windows.Forms.ToolStripStatusLabel();
            this.okayButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.daysBar = new System.Windows.Forms.ProgressBar();
            this.usageBar = new System.Windows.Forms.ProgressBar();
            this.signalBox = new System.Windows.Forms.GroupBox();
            this.signalBand = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.meteredDailyAveragesBox.SuspendLayout();
            this.meteredUsageBox.SuspendLayout();
            this.planBox.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.signalBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // meteredDailyAveragesBox
            // 
            this.meteredDailyAveragesBox.Controls.Add(this.soFarMonth);
            this.meteredDailyAveragesBox.Controls.Add(this.startOfMonth);
            this.meteredDailyAveragesBox.Controls.Add(this.label8);
            this.meteredDailyAveragesBox.Controls.Add(this.label5);
            this.meteredDailyAveragesBox.Controls.Add(this.remainingDailyUsage);
            this.meteredDailyAveragesBox.Controls.Add(this.label7);
            this.meteredDailyAveragesBox.Location = new System.Drawing.Point(12, 207);
            this.meteredDailyAveragesBox.Name = "meteredDailyAveragesBox";
            this.meteredDailyAveragesBox.Size = new System.Drawing.Size(270, 83);
            this.meteredDailyAveragesBox.TabIndex = 1;
            this.meteredDailyAveragesBox.TabStop = false;
            this.meteredDailyAveragesBox.Text = "Metered Daily Averages";
            // 
            // soFarMonth
            // 
            this.soFarMonth.AutoSize = true;
            this.soFarMonth.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.soFarMonth.Location = new System.Drawing.Point(183, 36);
            this.soFarMonth.Name = "soFarMonth";
            this.soFarMonth.Size = new System.Drawing.Size(35, 13);
            this.soFarMonth.TabIndex = 5;
            this.soFarMonth.Text = "sofar";
            this.soFarMonth.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // startOfMonth
            // 
            this.startOfMonth.AutoSize = true;
            this.startOfMonth.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.startOfMonth.Location = new System.Drawing.Point(183, 21);
            this.startOfMonth.Name = "startOfMonth";
            this.startOfMonth.Size = new System.Drawing.Size(32, 13);
            this.startOfMonth.TabIndex = 4;
            this.startOfMonth.Text = "start";
            this.startOfMonth.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 21);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(87, 13);
            this.label8.TabIndex = 3;
            this.label8.Text = "At start of month:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 37);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(89, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "So far this month:";
            // 
            // remainingDailyUsage
            // 
            this.remainingDailyUsage.AutoSize = true;
            this.remainingDailyUsage.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.remainingDailyUsage.Location = new System.Drawing.Point(183, 53);
            this.remainingDailyUsage.Name = "remainingDailyUsage";
            this.remainingDailyUsage.Size = new System.Drawing.Size(61, 13);
            this.remainingDailyUsage.TabIndex = 1;
            this.remainingDailyUsage.Text = "remaining";
            this.remainingDailyUsage.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 53);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(89, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "For rest of month:";
            // 
            // meteredUsageBox
            // 
            this.meteredUsageBox.Controls.Add(this.dataTransferred);
            this.meteredUsageBox.Controls.Add(this.label3);
            this.meteredUsageBox.Location = new System.Drawing.Point(13, 150);
            this.meteredUsageBox.Name = "meteredUsageBox";
            this.meteredUsageBox.Size = new System.Drawing.Size(269, 53);
            this.meteredUsageBox.TabIndex = 2;
            this.meteredUsageBox.TabStop = false;
            this.meteredUsageBox.Text = "Metered Usage";
            // 
            // dataTransferred
            // 
            this.dataTransferred.AutoSize = true;
            this.dataTransferred.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataTransferred.Location = new System.Drawing.Point(183, 22);
            this.dataTransferred.Name = "dataTransferred";
            this.dataTransferred.Size = new System.Drawing.Size(52, 13);
            this.dataTransferred.TabIndex = 2;
            this.dataTransferred.Text = "metered";
            this.dataTransferred.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Metered this month:";
            // 
            // planBox
            // 
            this.planBox.Controls.Add(this.nextBillingDate);
            this.planBox.Controls.Add(this.monthlyQuota);
            this.planBox.Controls.Add(this.label10);
            this.planBox.Controls.Add(this.label9);
            this.planBox.Location = new System.Drawing.Point(13, 295);
            this.planBox.Name = "planBox";
            this.planBox.Size = new System.Drawing.Size(269, 68);
            this.planBox.TabIndex = 3;
            this.planBox.TabStop = false;
            this.planBox.Text = "Plan";
            // 
            // nextBillingDate
            // 
            this.nextBillingDate.AutoSize = true;
            this.nextBillingDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nextBillingDate.Location = new System.Drawing.Point(183, 42);
            this.nextBillingDate.Name = "nextBillingDate";
            this.nextBillingDate.Size = new System.Drawing.Size(40, 13);
            this.nextBillingDate.TabIndex = 3;
            this.nextBillingDate.Text = "billing";
            this.nextBillingDate.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // monthlyQuota
            // 
            this.monthlyQuota.AutoSize = true;
            this.monthlyQuota.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.monthlyQuota.Location = new System.Drawing.Point(183, 20);
            this.monthlyQuota.Name = "monthlyQuota";
            this.monthlyQuota.Size = new System.Drawing.Size(39, 13);
            this.monthlyQuota.TabIndex = 2;
            this.monthlyQuota.Text = "quota";
            this.monthlyQuota.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 42);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(117, 13);
            this.label10.TabIndex = 1;
            this.label10.Text = "Next billing cycle starts:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 20);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(98, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "Monthly allowance:";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lastUpdatedStatus,
            this.lastUpdated});
            this.statusStrip1.Location = new System.Drawing.Point(0, 451);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(294, 22);
            this.statusStrip1.TabIndex = 4;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // lastUpdatedStatus
            // 
            this.lastUpdatedStatus.Name = "lastUpdatedStatus";
            this.lastUpdatedStatus.Size = new System.Drawing.Size(78, 17);
            this.lastUpdatedStatus.Text = "Last updated:";
            // 
            // lastUpdated
            // 
            this.lastUpdated.Name = "lastUpdated";
            this.lastUpdated.Size = new System.Drawing.Size(118, 17);
            this.lastUpdated.Text = "updated_placeholder";
            this.lastUpdated.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // okayButton
            // 
            this.okayButton.Location = new System.Drawing.Point(99, 423);
            this.okayButton.Name = "okayButton";
            this.okayButton.Size = new System.Drawing.Size(75, 23);
            this.okayButton.TabIndex = 5;
            this.okayButton.Text = "OK";
            this.okayButton.UseVisualStyleBackColor = true;
            this.okayButton.Click += new System.EventHandler(this.OkayButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(133, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Metered Usage Remaining";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 78);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(151, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Days in billing period remaining";
            // 
            // daysBar
            // 
            this.daysBar.Location = new System.Drawing.Point(12, 109);
            this.daysBar.Name = "daysBar";
            this.daysBar.Size = new System.Drawing.Size(270, 23);
            this.daysBar.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.daysBar.TabIndex = 8;
            this.daysBar.Value = 33;
            // 
            // usageBar
            // 
            this.usageBar.Location = new System.Drawing.Point(12, 40);
            this.usageBar.Name = "usageBar";
            this.usageBar.Size = new System.Drawing.Size(270, 23);
            this.usageBar.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.usageBar.TabIndex = 9;
            this.usageBar.Value = 33;
            // 
            // signalBox
            // 
            this.signalBox.Controls.Add(this.label4);
            this.signalBox.Controls.Add(this.signalBand);
            this.signalBox.Location = new System.Drawing.Point(12, 370);
            this.signalBox.Name = "signalBox";
            this.signalBox.Size = new System.Drawing.Size(270, 47);
            this.signalBox.TabIndex = 10;
            this.signalBox.TabStop = false;
            this.signalBox.Text = "Signal";
            // 
            // signalBand
            // 
            this.signalBand.AutoSize = true;
            this.signalBand.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.signalBand.Location = new System.Drawing.Point(56, 20);
            this.signalBand.Name = "signalBand";
            this.signalBand.Size = new System.Drawing.Size(35, 13);
            this.signalBand.TabIndex = 3;
            this.signalBand.Text = "band";
            this.signalBand.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(32, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Band";
            // 
            // UsageForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(294, 473);
            this.Controls.Add(this.signalBox);
            this.Controls.Add(this.usageBar);
            this.Controls.Add(this.daysBar);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.okayButton);
            this.Controls.Add(this.meteredDailyAveragesBox);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.planBox);
            this.Controls.Add(this.meteredUsageBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "UsageForm";
            this.Text = "Telstra 4GX Usage Monitor";
            this.Load += new System.EventHandler(this.UsageForm_Load);
            this.meteredDailyAveragesBox.ResumeLayout(false);
            this.meteredDailyAveragesBox.PerformLayout();
            this.meteredUsageBox.ResumeLayout(false);
            this.meteredUsageBox.PerformLayout();
            this.planBox.ResumeLayout(false);
            this.planBox.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.signalBox.ResumeLayout(false);
            this.signalBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.GroupBox meteredDailyAveragesBox;
        private System.Windows.Forms.GroupBox meteredUsageBox;
        private System.Windows.Forms.GroupBox planBox;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel lastUpdatedStatus;
        private System.Windows.Forms.Button okayButton;
        private System.Windows.Forms.Label remainingDailyUsage;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label dataTransferred;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label nextBillingDate;
        private System.Windows.Forms.Label monthlyQuota;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label soFarMonth;
        private System.Windows.Forms.Label startOfMonth;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ToolStripStatusLabel lastUpdated;
        private System.Windows.Forms.ProgressBar daysBar;
        private System.Windows.Forms.ProgressBar usageBar;
        private System.Windows.Forms.GroupBox signalBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label signalBand;
    }
}

