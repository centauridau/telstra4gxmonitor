﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using Newtonsoft.Json.Linq;

namespace Telstra4GXMonitor
{
    /*
     * Verified to work with NetGear Advanced III and Nighthawk M1
     */ 
    public class UsageData
    {
        // TODO Make this configurable/changeable - maybe extra tab?
        private const string NetgearUrl = "http://192.168.25.1/api/model.json?internalapi=1";

        public enum UsageStatus
        {
            Normal   = 0,
            Minimal  = 5,
            Minor    = 10,
            Notable  = 15,
            Major    = 20,
            Critical = 30,
            Yikes    = 50,
        }

        #region Properties

        public long MonthlyQuotaMb { get; private set; }
        public long DataRemainingMb { get; private set; }
        public long DataUsedMb { get; private set; }

        public TimeSpan TimeRemaining { get; private set; }
        public DateTime NextBillingDate { get; private set; }
        public DateTime LastUpdated { get; private set; }

        public long GpsNextBillingDate { get; private set; }
        public long GpsLastUpdated { get; private set; }

        public int PercentageUsed { get; private set; }
        public int PercentageRemaining { get; private set; }

        public int TimePercentageUsed { get; private set; }
        public int TimePercentageRemaining { get; private set; }

        public double DailyUsageRecommendedMb { get; private set; }
        public double DailyUsageRemainingMb { get; private set; }

        public double DataUsedSoFarPerDayMb { get; private set; }

        public int SignalBars { get; private set; }
        public int SignalQuality { get; private set; }
        public string SignalBand { get; private set; }

        public UsageStatus CurrentUsageStatus { get; private set; }
        // public int CurrentUsageStatus { get; private set; }

        #endregion

        #region Public Methods

        public UsageStatus UpdateValues()
        // public int UpdateValues()
        {
            // Get the JSON from the device then parse it into a nice structure
            var netGearJson = GetNetGearJson();
            var jsonData = JObject.Parse(netGearJson);

            // Pluck values from the JSON
            var dataUsed = long.Parse((string) jsonData["wwan"]["dataUsage"]["generic"]["dataTransferred"]);
            var monthlyQuota = long.Parse((string) jsonData["wwan"]["dataUsage"]["generic"]["billingCycleLimit"]);
            var gpsTimeRemaining = long.Parse((string) jsonData["wwan"]["dataUsage"]["generic"]["billingCycleRemainder"]);

            SignalBars = int.Parse((string) jsonData["wwan"]["signalStrength"]["bars"]);
            SignalQuality = int.Parse((string) jsonData["wwanadv"]["radioQuality"]);
            var signalBand = (string) jsonData["wwanadv"]["curBand"];
            var bandFrequency = new Dictionary<string, int>()
            {
                ["LTE B28"] =  700,
                ["LTE B3"] = 1800,
                ["LTE B7"] =  2600,
            };
            SignalBand = $"{signalBand}/{bandFrequency[signalBand]}";


            GpsLastUpdated = long.Parse((string) jsonData["wwan"]["dataUsage"]["generic"]["lastSync"]);
            GpsNextBillingDate = long.Parse((string) jsonData["wwan"]["dataUsage"]["generic"]["nextBillingDate"]);

            var daysInCurrentMonth = DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month);
            var secondsInMonth = daysInCurrentMonth * 86400;
            var secondsUsed = secondsInMonth - gpsTimeRemaining;

            var dataRemaining = monthlyQuota - dataUsed;
            var dailyUsageRecommended = (double)monthlyQuota / (double)daysInCurrentMonth;
            var dailyUsageRemaining = (double)dataRemaining / ((double)gpsTimeRemaining / 86400.0);
            var dataUsedSoFarPerDay = (double) dataUsed / ((double)secondsUsed / 86400.0);

            LastUpdated = TimeFromGpsEpoch(GpsLastUpdated);
            NextBillingDate = TimeFromGpsEpoch(GpsNextBillingDate);
            TimeRemaining = TimeSpan.FromSeconds(gpsTimeRemaining);

            MonthlyQuotaMb = monthlyQuota / (1024 * 1024);

            DataUsedMb = dataUsed / (1024 * 1024);
            DataUsedSoFarPerDayMb = dataUsedSoFarPerDay / (1024 * 1024);

            DataRemainingMb = dataRemaining / (1024 * 1024);
            DailyUsageRecommendedMb = dailyUsageRecommended / (1024 * 1024);
            DailyUsageRemainingMb = dailyUsageRemaining / (1024 * 1024);

            PercentageUsed = (int) ((double)dataUsed / (double)monthlyQuota * 100.0);
            PercentageRemaining = (int) (100.0 - PercentageUsed);

            TimePercentageUsed = (int) ((double)secondsUsed / (double)secondsInMonth * 100.0);
            TimePercentageRemaining = (int) (100.0 - TimePercentageUsed);

            CurrentUsageStatus =  CalculateUsageStatus(PercentageUsed, TimePercentageUsed);
            return CurrentUsageStatus;
        }

        #endregion

        #region Private Methods

        private static UsageStatus CalculateUsageStatus(int percentageUsed, int timePercentageUsed)
        // private static int CalculateUsageStatus(int percentageUsed, int timePercentageUsed)
        {
            var overUse = percentageUsed - timePercentageUsed;
            // return overUse;
            if (overUse > 0 && overUse < (int) UsageStatus.Major)
            {
                return UsageStatus.Minor;
            }
            else if (overUse >= (int) UsageStatus.Major && overUse < (int) UsageStatus.Critical)
            {
                return UsageStatus.Major;
            }
            else if (overUse >= (int) UsageStatus.Critical)
            {
                return UsageStatus.Critical;
            }
            else
            {
                return UsageStatus.Normal;
            }
        }

        private static string GetNetGearJson()
        {
            using (var httpClient = new HttpClient())
            {
                var response = httpClient.GetAsync(NetgearUrl).Result;
                var result = response.Content.ReadAsStringAsync().Result;

                return result;
            }
        }

        private static DateTime TimeFromGpsEpoch(long seconds)
        {
            var gpsEpoch = new DateTime(1980, 1, 6, 0, 0, 0);
            var time = gpsEpoch.AddSeconds(seconds);
            return time;
        }

        #endregion
    }
}
